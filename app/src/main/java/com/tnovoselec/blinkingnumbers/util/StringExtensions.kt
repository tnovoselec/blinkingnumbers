package com.tnovoselec.blinkingnumbers.util

import java.math.BigDecimal

fun String.toBigDecimalSafe(): BigDecimal {
    return if (this.isEmpty()) BigDecimal.ZERO else BigDecimal(this)
}