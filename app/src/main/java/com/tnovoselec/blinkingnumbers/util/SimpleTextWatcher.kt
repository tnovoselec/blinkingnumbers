package com.tnovoselec.blinkingnumbers.util

import android.text.Editable
import android.text.TextWatcher

class SimpleTextWatcher(private val textChanged: (String) -> Unit) : TextWatcher {
    override fun afterTextChanged(s: Editable) {
        textChanged(s.toString())
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
    }
}