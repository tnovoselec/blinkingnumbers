package com.tnovoselec.blinkingnumbers.model

import android.databinding.BaseObservable
import android.databinding.Bindable

class SumResult(private var result: String) : BaseObservable() {

    @Bindable
    fun getResult(): String {
        return result
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SumResult

        if (result != other.result) return false

        return true
    }

    override fun hashCode(): Int {
        return result.hashCode()
    }


}