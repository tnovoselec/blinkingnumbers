package com.tnovoselec.blinkingnumbers.business

import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit

class RxFlasher(val observeScheduler: Scheduler,
                val subscribeScheduler: Scheduler) : Flasher {

    private val disposable: CompositeDisposable = CompositeDisposable()
    override var listener: Flasher.OnFlashListener? = null

    override fun initiateFlashing() {
        disposable.clear()
        disposable.add(Flowable.interval(500, TimeUnit.MILLISECONDS)
                .flatMap { Flowable.just(it % 2 == 0L) }
                .subscribeOn(subscribeScheduler)
                .observeOn(observeScheduler)
                .subscribe { listener?.shouldFlash(it) })
    }

    override fun stopFlashing() {
        disposable.clear()
        listener?.shouldFlash(true)
    }
}