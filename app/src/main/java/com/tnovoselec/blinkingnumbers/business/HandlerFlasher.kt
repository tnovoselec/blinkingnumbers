package com.tnovoselec.blinkingnumbers.business

import android.os.Handler

class HandlerFlasher() : Flasher {

    private val handler = Handler()
    override var listener: Flasher.OnFlashListener? = null

    private var nextEvent = true

    override fun initiateFlashing() {
        flashingRunnable.run()
    }

    override fun stopFlashing() {
        handler.removeCallbacksAndMessages(null)
    }


    private val flashingRunnable = object : Runnable {
        override fun run() {
            listener?.shouldFlash(nextEvent)
            handler.postDelayed(this, 500)
            nextEvent = !nextEvent
        }

    }
}