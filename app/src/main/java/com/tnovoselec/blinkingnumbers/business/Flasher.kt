package com.tnovoselec.blinkingnumbers.business

interface Flasher {

    interface OnFlashListener {
        fun shouldFlash(flash: Boolean)
    }

    var listener: OnFlashListener?

    fun initiateFlashing()

    fun stopFlashing()
}