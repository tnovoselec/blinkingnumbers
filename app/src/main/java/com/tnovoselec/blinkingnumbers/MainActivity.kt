package com.tnovoselec.blinkingnumbers

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.tnovoselec.blinkingnumbers.databinding.ActivityMainBinding
import com.tnovoselec.blinkingnumbers.util.SimpleTextWatcher
import com.tnovoselec.blinkingnumbers.util.toBigDecimalSafe
import com.tnovoselec.blinkingnumbers.viewmodel.NumbersViewModel
import com.tnovoselec.blinkingnumbers.viewmodel.NumbersViewModelFactory
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var numbersViewModelFactory: NumbersViewModelFactory

    private lateinit var binding: ActivityMainBinding
    private lateinit var numbersViewModel: NumbersViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        numbersViewModel = ViewModelProviders.of(this, numbersViewModelFactory).get(NumbersViewModel::class.java)
        observeInputFields()
        observeSum()
        observeFlashing()
    }

    fun observeInputFields() {
        val inputFields = arrayOf(binding.firstNumber, binding.secondNumber, binding.thirdNumber, binding.fourthNumber, binding.fifthNumber, binding.sixthNumber)
        inputFields.forEachIndexed { index, field ->
            field.addTextChangedListener(SimpleTextWatcher { numbersViewModel.onNumberChanged(index, it.toBigDecimalSafe()) })
        }

        binding.sumResult.setOnClickListener { numbersViewModel.onFlashingRequested() }

    }

    fun observeSum() {
        numbersViewModel.sum.subscribe { binding.sum = it }
    }

    fun observeFlashing() {
        numbersViewModel.flashingProcessor.subscribe { binding.sumResult.visibility = if (it) View.VISIBLE else View.INVISIBLE }
    }

    fun inject() {
        AndroidInjection.inject(this)
    }
}
