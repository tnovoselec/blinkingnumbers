package com.tnovoselec.blinkingnumbers.viewmodel

import android.arch.lifecycle.ViewModel
import com.tnovoselec.blinkingnumbers.business.Flasher
import com.tnovoselec.blinkingnumbers.model.SumResult
import io.reactivex.processors.PublishProcessor
import java.math.BigDecimal
import java.text.NumberFormat

class NumbersViewModel(val flasher: Flasher) : ViewModel(), Flasher.OnFlashListener {

    val formatter = NumberFormat.getInstance()
    val sum = PublishProcessor.create<SumResult>()
    val flashingProcessor = PublishProcessor.create<Boolean>()
    val numbers = HashMap<Int, BigDecimal>()

    var isFlashing = false

    init {
        flasher.listener = this
    }

    fun onNumberChanged(index: Int, value: BigDecimal) {
        numbers[index] = value
        val result = numbers.values.reduce { acc, bigDecimal -> acc.plus(bigDecimal) }
        sum.onNext(SumResult(formatter.format(result)))
    }

    fun onFlashingRequested() {
        if (isFlashing) {
            flasher.stopFlashing()
        } else {
            flasher.initiateFlashing()
        }
        isFlashing = !isFlashing
    }

    override fun shouldFlash(flash: Boolean) {
        flashingProcessor.onNext(flash)
    }

}