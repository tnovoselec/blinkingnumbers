package com.tnovoselec.blinkingnumbers.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.tnovoselec.blinkingnumbers.business.Flasher


class NumbersViewModelFactory (val flasher: Flasher) : ViewModelProvider.Factory  {


    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return NumbersViewModel(flasher) as T
    }
}