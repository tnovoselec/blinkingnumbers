package com.tnovoselec.blinkingnumbers.dagger

import com.tnovoselec.blinkingnumbers.business.Flasher
import com.tnovoselec.blinkingnumbers.viewmodel.NumbersViewModelFactory
import dagger.Module
import dagger.Provides


@Module
class ActivityModule {


    @Provides
    internal fun provideNumbersViewModelFactory(flasher: Flasher) = NumbersViewModelFactory(flasher)

}