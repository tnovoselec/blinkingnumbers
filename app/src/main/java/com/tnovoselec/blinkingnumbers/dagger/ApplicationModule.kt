package com.tnovoselec.blinkingnumbers.dagger

import com.tnovoselec.blinkingnumbers.BuildConfig
import com.tnovoselec.blinkingnumbers.business.Flasher
import com.tnovoselec.blinkingnumbers.business.HandlerFlasher
import com.tnovoselec.blinkingnumbers.business.RxFlasher
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


@Module
class ApplicationModule {

    @Provides
    // internal fun provideFlasher(): Flasher =
    internal fun provideFlasher(): Flasher =
            if (BuildConfig.USE_RX_FLASHER) {
                RxFlasher(AndroidSchedulers.mainThread(), Schedulers.io())
            } else {
                HandlerFlasher()
            }

}