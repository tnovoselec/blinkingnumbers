package com.tnovoselec.blinkingnumbers.dagger

import com.tnovoselec.blinkingnumbers.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [ActivityModule::class])
    abstract fun bindMainActivity() : MainActivity
}