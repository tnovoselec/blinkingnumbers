package com.tnovoselec.blinkingnumbers.dagger

import com.tnovoselec.blinkingnumbers.BlinkingNumbersApplication
import dagger.BindsInstance
import dagger.Component
import dagger.MembersInjector
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            ApplicationModule::class,
            AndroidInjectionModule::class,
            ActivityBuilder::class
        ]
)
interface ApplicationComponent : MembersInjector<BlinkingNumbersApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(blinkingNumbersApplication: BlinkingNumbersApplication): Builder

        fun build(): ApplicationComponent
    }
}