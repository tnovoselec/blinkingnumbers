package com.tnovoselec.blinkingnumbers

import com.tnovoselec.blinkingnumbers.business.Flasher
import com.tnovoselec.blinkingnumbers.model.SumResult
import com.tnovoselec.blinkingnumbers.viewmodel.NumbersViewModel
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import java.math.BigDecimal


@RunWith(MockitoJUnitRunner::class)
class NumbersViewModelTest {

    @Mock
    lateinit var flasher: Flasher

    private lateinit var numbersViewModel: NumbersViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        numbersViewModel = NumbersViewModel(flasher)
    }

    @Test
    fun testOnNumberChanged() {

        val testSubscriber = numbersViewModel.sum.test()

        numbersViewModel.onNumberChanged(0, BigDecimal.valueOf(1))
        numbersViewModel.onNumberChanged(1, BigDecimal.valueOf(2))
        numbersViewModel.onNumberChanged(2, BigDecimal.valueOf(3))

        numbersViewModel.sum.onComplete()

        testSubscriber.assertResult(SumResult("1"), SumResult("3"), SumResult("6"))

    }

    @Test
    fun testOnFlashingRequested() {

        Mockito.`when`(flasher.initiateFlashing()).then {
            numbersViewModel.shouldFlash(true)
            numbersViewModel.shouldFlash(false)
            numbersViewModel.shouldFlash(true)
        }

        val testSubscriber = numbersViewModel.flashingProcessor.test()

        numbersViewModel.onFlashingRequested()

        numbersViewModel.flashingProcessor.onComplete()

        testSubscriber.assertValues(true, false, true)


    }

}
